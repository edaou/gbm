## GBM generator

Generates geometric Brownian motion sample data and plots.

#### Dependencies
- POSIX
- C++11 compiler (gcc is used by default)
- GNU make
- gnuplot

#### Project structure
- `src/` contains the GBM data generator source code.
- `resources/` contains a gnuplot script for generating plots.
- `out/` contains the program output:
    - `gbm` : compiled GBM data generator.
    - `data/` : stores GBM data as text files.
    - `img/` : stores GBM plots as png files.

#### Usage
##### Building the data generator
`make build` compiles the GBM data generator.

##### Building the data generator with another C++11 compiler
`make build CC=clang++` uses clang++ to compile the data generator.

##### Generate default data and plot
`make gendata` produces sample data for a 10-second 0-drift 1-volatility GBM
process (timestep size = 0.0001).

`make genplot` calls `gendata` then generates a plot for the obtained process.

##### Customize GBM parameters
`make gendata time=30 ts=0.001 drift=0.5 vol=0.25` produces sample data 
for a 30-second GBM process with a drift of 0.5 and a volatility of 0.25
(timestep size = 0.001).

`make genplot time=30 ts=0.001 drift=0.5 vol=0.25` calls `gendata` with the passed-in parameters then generates a plot for the obtained process.

##### Rebuild the project after modifying the data generator.
`make clean build`

:warning: `make clean` will delete the `out/` directory, which might contain previously generated data and plots.