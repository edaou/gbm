# program dependencies
CC=g++
CPP_ARGS?=-O3 -Wall -Werror -std=c++11 -lm

GP=gnuplot

# simulation arguments
duration?=30.0
ts?=0.0001
drift?=0
vol?=1
date:=$(shell date --utc +%s%3N)
filename=w${duration}_${ts}_${date}
GP_ARGS="filename='${filename}'; duration=${duration}; ts=${ts}; drift=${drift}; vol=${vol}"

# project structure
OUTPUT_DIR=out
DATA_DIR=${OUTPUT_DIR}/data
IMG_DIR=${OUTPUT_DIR}/img
RESOURCES_DIR=resources
SRC=src

.PHONY: clean build gendata genplot all

all: clean build gendata genplot

clean:
	rm -rf ${OUTPUT_DIR}

build:
	mkdir -p ${OUTPUT_DIR} ${IMG_DIR} ${DATA_DIR}
	$(CC) ${CPP_ARGS} -o ${OUTPUT_DIR}/gbm ${SRC}/gbm.cpp

gendata:
	./${OUTPUT_DIR}/gbm $(duration) $(ts) $(drift) $(vol) \
	> ${DATA_DIR}/${filename}.dat

genplot: gendata
	${GP} -e ${GP_ARGS} ${RESOURCES_DIR}/gbm.gp
