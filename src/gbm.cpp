#include <cmath>
#include <iostream>
#include <random>
#include <string>

using namespace std;

class RandomWalk
{
public:
  explicit RandomWalk(double t, double ts) :
    RandomWalk(static_cast<size_t>(abs(t/ts)))
  {
  }

  explicit RandomWalk(size_t n) :
    m_n(n),
    m_i(0),
    m_sample(0.0),
    m_distribution(0.5),
    m_rng(random_device()())
  {
  }

  RandomWalk(const RandomWalk&) = default;
  RandomWalk(RandomWalk&&) = default;
  RandomWalk& operator=(const RandomWalk&) = default;
  RandomWalk& operator=(RandomWalk&&) = default;

  double operator()() noexcept
  {
    if (m_i++ > m_n) return 0;
    int Xi = 2 * (static_cast<int>(m_distribution(m_rng)) - 0.5);
    m_sample += Xi/sqrt(m_n);
    return m_sample;
  }

private:
  size_t m_n;
  size_t m_i;
  double m_sample;
  bernoulli_distribution m_distribution;
  mt19937 m_rng;
};

int main(int argc, char* argv[])
{
  if (argc < 3) {
    cerr << argv[0] << " period timestep_size [drift=0.0] [vol=1.0]\n";
    return 1;
  }

  const double T = stod(argv[1]);
  const double ts = stod(argv[2]);
  const double mu = (argc >= 4) ? stod(argv[3]) : 0.0;
  const double sigma = (argc >= 5) ? stod(argv[4]) : 1.0;

  cerr
    << "--------------------------------------------------------------------"
    << "\nGBM parameters:"
    << "\nduration: " << T
    << "\ntimestep size: " << ts
    << "\nmu: " << mu
    << "\nsigma: " << sigma
    << "\n--------------------------------------------------------------------\n";

  RandomWalk walk(T, ts);

  double timestep = 0.0, value = 0.0;
  for (int i = 0; i < T/ts; ++i) {
    timestep = i * ts;
    value = exp(sigma * walk() + mu * timestep);
    cout << timestep << " " << value  << '\n';
  }
  return 0;
}
