#!/usr/bin/gnuplot

plot_title = sprintf("Geometric Brownian Motion\nT=%.1f, P=%.5f, μ=%.2f, σ=%.2f", duration, ts, drift, vol) 

data_filename= sprintf("out/data/%s.dat", filename)
output_filename = sprintf("out/img/%s.png", filename)

set title plot_title
set xlabel 't'

set grid
set key below center horizontal noreverse enhanced autotitle box dashtype solid
set border 3 front linetype black linewidth 1.0 dashtype solid

set style line 1 linecolor rgb '#0060ad' linetype 1 linewidth 1

set terminal png enhanced
set output output_filename
plot data_filename using 1:2 with lines linestyle 1 title 'process'
